"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
const express = require("express");
const app = express();
//======[Включаем хистори мод (у нас SPA, поэтому он нам нужен)]============
const connect_history_api_fallback_1 = __importDefault(require("connect-history-api-fallback")); //history mode
app.use(connect_history_api_fallback_1.default());
const server = app.listen(process.env.PORT, () => {
    console.log("server up and running on PORT :", process.env.PORT);
});
app.use("/", express.static("./build"));
