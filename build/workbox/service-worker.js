import {registerRoute} from 'workbox-routing';
import {CacheFirst} from 'workbox-strategies';
import {CacheableResponsePlugin} from 'workbox-cacheable-response';

registerRoute(
    ({url}) => {console.log(url); return !!url.pathname.match(".glb")},
    new CacheFirst({
        cacheName: 'model-cache',
        plugins: [
            new CacheableResponsePlugin({
                statuses: [200],
            })
        ]
    })
);
