importScripts("/precache-manifest.e38458f075c08c883763fcd6952d8fa8.js", "https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */


importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

let routeRegular = /(\.glb|\.jpg|\.png|\.svg|\.ico|\.woff|\.css|)/i

workbox.routing.registerRoute(
    ({url}) => routeRegular.test(url.pathname),
    new workbox.strategies.CacheFirst({
        cacheName: 'model-cache',
    })
);




workbox.core.setCacheNameDetails({prefix: "threejs-test"});

self.addEventListener('message', (event) => {
    if (event.data && event.data.type === 'SKIP_WAITING') {
        self.skipWaiting();
    }
});

