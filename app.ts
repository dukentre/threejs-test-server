require("dotenv").config()

const express = require("express")

const app = express()

//======[Включаем хистори мод (у нас SPA, поэтому он нам нужен)]============
import history from 'connect-history-api-fallback';//history mode
app.use(history());

const server = app.listen(process.env.PORT, () => {
    console.log("server up and running on PORT :", process.env.PORT);
});

app.use("/",express.static("./build"));
